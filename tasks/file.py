import shutil
from pathlib import Path

from settings import config
from utils import file


def make_file(file_name, job_id):
    file.make_file_job(file_name, job_id)


def move_file(name):
    old_file = file.file_name(config.TASK_PATH_ACTIVE, name)
    new_file = file.file_name(config.TASK_PATH_IN_ACTIVE, name)
    shutil.move(old_file, new_file)
