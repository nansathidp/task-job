from rq_scheduler import Scheduler


class TaskScheduler(Scheduler):
    def enqueue_at(self, scheduled_time, func, *args, **kwargs):
        scheduled_time = scheduled_time.utcfromtimestamp(scheduled_time.timestamp())
        return super().enqueue_at(scheduled_time, func, *args, **kwargs)
