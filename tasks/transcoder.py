from shares import service, trascode


def action(code, is_start):
    cached = service.Cached()
    data = cached.memory.get(code, None)
    if isinstance(data, dict):
        params = {
            'id': data.get('device_id'),
            'chidx': data.get('channel_index')
        }
        ip = cached.memory.get('ip_active')
        if is_start:
            _action = 'start'
        else:
            _action = 'stop'
        body = {
            'operation': 'record', 'action': _action
        }

        trascode.record(ip, params, body)
    else:
        print('{} is {}'.format(code, data))
