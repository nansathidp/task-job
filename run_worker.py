from redis import Redis
from rq import Connection, Worker

from settings import config


def main():
    redis = Redis(config.RQ.get('HOST'), config.RQ.get('PORT'))
    with Connection(connection=redis):
        w = Worker('default')
        w.work()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Stop worker.')
