#!/bin/env python
import sys

from redis import Redis
from rq import Queue
from rq_scheduler import Scheduler

from settings import config

redis = Redis(config.RQ.get('HOST'), config.RQ.get('PORT'))
scheduler = Scheduler(connection=redis)
queue = Queue(connection=redis)

from datetime import datetime, timedelta
from shares.task import create_task
from uuid import uuid4


def read_jobs():
    for job, start_time in scheduler.get_jobs(with_times=True):
        print(job, start_time)


def create_job(code):
    datetime_start = datetime.now() + timedelta(minutes=1)
    datetime_end = datetime_start + timedelta(minutes=3)
    ep_id = uuid4().hex
    create_task(code, datetime_start, datetime_end, ep_id)
    print('create job {} start {} to {}'.format(ep_id, datetime_start.strftime('%X'), datetime_end.strftime('%X')))


def action_record(code, is_start):
    # print(code, is_start)
    from tasks.transcoder import action
    action(code, is_start)


def fetch_channel():
    channel_list = []
    from shares.service import Cached
    service = Cached()
    channel_all = service.memory.get('information_all', [])
    for device in channel_all:
        for channel in device['ch_info']:
            name = channel['name']
            name = name.split('_')[0]
            if 'Live' in name or 'Standby' in name:
                continue
            channel.update({'name': name})
            channel_list.append(channel)
    return channel_list


def call_execute(func, code_list, **kwargs):
    from shares.service import Cached
    service = Cached()
    for code in code_list:
        if service.memory.get(code, None):
            func(code, **kwargs)


def manage_channel(options, code_list):
    try:

        channel_list = fetch_channel()
        if options == 'ps':
            channel_table(channel_list)
        elif options == 'init':
            call_execute(create_job, code_list)
        elif options in ['start', 'stop']:
            call_execute(action_record, code_list, is_start=options == 'start')
        else:
            command_channel_show()
    except Exception as err:
        print('Connection error')


def channel_table(channel_list):
    print('ID\t\tNAME\t\tRECORD\t\tIP ADDRESS')
    from shares.service import Cached
    service = Cached()
    ip = service.memory.get('ip_active', None)
    for channel in channel_list:
        print('{}\t\t{}\t\t{}\t\t'.format(
            channel['id'], channel['name'],
            bool(channel['isrecording']),
            ip)
        )


def command_show():
    print('--------- Record manage ----------------')
    print('channel\t\tAction to channel manage')
    print('job\t\tAction to job manage')


def command_channel_show():
    print('--------- Record manage channel ----------------')
    print('ps\t\tProcess list channel')
    print('start\t\tStart record to channel')
    print('stop\t\tStop record to channel')
    print('init\t\tInit test create job record to channel (3 minutes)')
    print('count\t\tStop record to channel')


if __name__ == '__main__':
    try:
        input_args = sys.argv
        input_args.pop(0)
        cmd = input_args.pop(0)
        option = input_args.pop(0)
        if 'channel' == cmd:
            manage_channel(option, input_args)
        elif 'job' == cmd:
            if option == 'ps':
                read_jobs()
        else:
            command_show()
    except Exception as err:
        print(err)
        command_show()
