import os

TRANSCODE_IP = ['10.18.141.128', '10.18.12.27']

DATABASE = {
    'NAME': os.environ.get('ORACLE_NAME', 'ORCLPDB1'),
    'USER': os.environ.get('ORACLE_USER', 'obmsapp'),
    'PASSWORD': os.environ.get('ORACLE_PASSWORD', 'obms$user_01'),
    'PORT': os.environ.get('ORACLE_PORT', '1522'),
    'SID': os.environ.get('ORACLE_SID', 'obmsdb'),
    'HOST': os.environ.get('ORACLE_HOST', '10.18.14.234')
}

MONGODB_PORT = os.getenv('MONGODB_PORT', 31107)
MONGODB = {
    'HOST': os.getenv('MONGODB_HOST', '10.18.14.253'),
    'PORT': int(MONGODB_PORT),
    'NAME': os.getenv("MONGODB_NAME", 'stm')
}

CACHED = {
    'HOST': os.getenv('CACHED_HOST', '127.0.0.1'),
    'PORT': int(os.getenv('CACHED_PORT', 11211))
}

RQ = {
    'HOST': os.getenv('RQ_HOST', '127.0.0.1'),
    'PORT': int(os.getenv('RQ_PORT', 6379)),
}

BASE_PATH = os.getcwd()

TASK_PATH_ACTIVE = os.environ.get('TASK_ACTIVE', 'active/')
TASK_PATH_IN_ACTIVE = os.environ.get('TASK_IN_ACTIVE', 'inactive/')
TIME_OUT_STOP_RECORD = 30  # seconds

DATETIME_ORC_FORMAT = 'YYYY-MM-DD HH24:MI:SS'

DATETIME_FORMAT = '%Y-%m-%d %X'
DATETIME_FORMAT_FILE = '%Y%m%d%H%M%S'
TIME_FORMAT = '%H:%M'
