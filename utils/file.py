import json
import os


def file_name(path, name):
    return '%s%s' % (path, name)


def make_file_json(path, name, data):
    if isinstance(data, dict):
        with open(file_name(path, name), '+w') as file:
            json.dump(data, file)
    # else:
        # return None


def make_file_job(name, job_id):
    if isinstance(job_id, str):
        with open(name, '+w') as file:
            file.write(job_id)


def read_file_job( name):
    if os.path.exists(name):
        with open(file_name(name), 'r') as file:
            return file.read()
    return None


def read_file_json(path, name):
    if os.path.exists(file_name(path, name)):
        with open(file_name(path, name)) as file:
            return json.load(file)

    return None
