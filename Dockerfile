FROM python:3.7.4-slim-stretch
MAINTAINER nansathidp@gmail.com
WORKDIR /
ADD pkg .
ADD requirements.txt requirements.txt
RUN apt-get update && apt-get install -y alien build-essential unzip python3-dev libaio-dev vim \
  && alien oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm  \
  && alien oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm \
  && dpkg -i oracle-instantclient12.2-basic_12.2.0.1.0-2_amd64.deb \
  && dpkg -i oracle-instantclient12.2-sqlplus_12.2.0.1.0-2_amd64.deb \
  && unzip -a instantclient-basic-linux.x64-12.2.0.1.0.zip \
  && unzip -a instantclient-sdk-linux.x64-12.2.0.1.0.zip 

ENV ORACLE_HOME /instantclient_12_2/
ENV LD_LIBRARY_PATH /instantclient_12_2:$LD_LIBRARY_PATH
RUN ln -s  /instantclient_12_2/libclntsh.so.11.1 /instantclient_12_2/libclntsh.so \
  && ldconfig \
  && pip install -r requirements.txt \
  && mkdir -p /code/ \
  && mkdir -p /file/ \
  && mkdir -p /code/active/ \
  && mkdir -p /code/inactive/ \
  && ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && dpkg-reconfigure -f noninteractive tzdata \
  && echo "Asia/Bangkok" > /etc/timezone && rm /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
ADD . /code
WORKDIR /code/
