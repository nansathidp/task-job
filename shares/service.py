import cx_Oracle
from pymemcache import Client, serde
from pymongo import MongoClient


from settings import config


class MongoDB:
    def __init__(self):
        client = MongoClient(config.MONGODB.get('HOST'), config.MONGODB.get('PORT'))
        self.db = client.get_database(config.MONGODB.get('NAME'))


class Cached:

    def __init__(self):
        self._config = (config.CACHED.get('HOST'), config.CACHED.get('PORT'))
        self._set_up()

    def _set_up(self):
        self.memory = Client(
            self._config,
            serializer=serde.get_python_memcache_serializer(pickle_version=2),
            deserializer=serde.python_memcache_deserializer
        )


class Oracle:
    def __init__(self):

        self._cursor = None
        self.items = []

        dsn_tns = cx_Oracle.makedsn(
            config.DATABASE.get('HOST'),
            config.DATABASE.get('PORT'),
            service_name=config.DATABASE.get('SID')
        )
        self._connect = cx_Oracle.connect(
            user=config.DATABASE.get('USER'),
            password=config.DATABASE.get('PASSWORD'),
            dsn=dsn_tns,
            encoding='UTF-8',
        )
        self._err = None

    @property
    def load(self):

        self._cursor = self._connect.cursor()
        return self

    def exec(self, sql):
        try:
            _raw = self._cursor.execute(sql.upper())
            if 'SELECT ' in sql.upper():
                self._fetch(_raw)
        except Exception as err:
            self._err = err
        self._destroy()
        return self

    def _fetch(self, raw):
        if hasattr(raw, 'fetchall'):
            for item in raw.fetchall():
                self.items.append(item)

    @property
    def error(self):
        return self._err

    def _destroy(self):
        if hasattr(self._cursor, 'close'):
            self._cursor.close()
        if hasattr(self._connect, 'commit'):
            self._connect.commit()
        if hasattr(self._connect, 'close'):
            self._connect.close()




