import os
import sys

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
from settings import config
from shares import service, task

mongo_db = service.MongoDB()


def make_sql_by_select(datetime_start, datetime_end, code):
    datetime_start_str = datetime_start.strftime(config.DATETIME_FORMAT)
    datetime_end_str = datetime_end.strftime(config.DATETIME_FORMAT)
    code_upper = code.upper()
    # print(datetime_start, '---> ', datetime_end, '---> ', code_upper)
    return "SELECT episode_id, start_date, end_date, channel_code FROM SCH_BMS WHERE channel_code='{}'" \
           "AND START_DATE BETWEEN " \
           "TO_DATE('{}', '{}') AND TO_DATE('{}', '{}')" \
           "".format(
        code_upper,
        datetime_start_str, config.DATETIME_ORC_FORMAT,
        datetime_end_str, config.DATETIME_ORC_FORMAT
    )


def make_task(items, type_live_to_vod, channel_code):
    for item in items:
        ep_id = item[0]
        start_date = item[1]
        end_date = item[2]

        if type_live_to_vod == 'select':
            epg = mongo_db.db.live_record.find_one({'status': 'queue', 'ep_id': ep_id})
            if epg:
                print(ep_id, ' start_run ', start_date, ' type ', 'select', channel_code)
                task.create_task(channel_code, start_date, end_date, ep_id)
        else:
            print(ep_id, ' start_run ', start_date, 'type ', 'select')
            task.create_task(channel_code, start_date, end_date, ep_id)


def get_epg(datetime_start, datetime_end):
    channel_list = mongo_db.db.content_channel.find({'live_to_vod': {'$in': ['select', 'all']}})
    channel_list = list(channel_list)

    if len(channel_list) > 0:
        for channel in channel_list:
            type_live_to_vod = channel.get('live_to_vod', 'n/a')
            bms_channel_code = channel.get('bms_channel_code')
            if type_live_to_vod in ['select', 'all'] and bms_channel_code:
                sql = make_sql_by_select(datetime_start, datetime_end, bms_channel_code)
                oracle = service.Oracle()
                oracle.load.exec(sql)
                if oracle.error:
                    print('ERROR: ', oracle.error)
                else:
                    make_task(oracle.items, type_live_to_vod, channel.get('code'))
            else:
                print("{} validate bms_code".format(channel.get('code', None)))


def run():
    from datetime import datetime, timedelta
    datetime_start = datetime.now()
    datetime_end = datetime_start + timedelta(hours=1)
    get_epg(datetime_start, datetime_end)
    datetime_temp = datetime.now()
    datetime_start_midnight = datetime_temp.replace(hour=23, minute=35, second=35)
    datetime_stop_midnight = datetime_start_midnight + timedelta(minutes=45)
    get_epg(datetime_start_midnight, datetime_stop_midnight)


if __name__ == '__main__':
    run()
