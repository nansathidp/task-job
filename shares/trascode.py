import requests

from settings import config
from shares import service


def get_url(ip, path):
    return 'http://{}{}'.format(ip, path)


def get_ip_address(index):
    cached = service.Cached()
    ip_manage = config.TRANSCODE_IP[index]
    ip_manage_memory = cached.memory.get('ip_active', None)
    if ip_manage_memory in config.TRANSCODE_IP:
        ip_manage = ip_manage_memory
    return ip_manage


def action(code):
    pass


def set_active_ip(ip):
    cached = service.Cached()
    cached.memory.set('ip_active', ip)


def clear():
    cached = service.Cached()
    cached.memory.delete('ip_active')


def check_active(ip):
    response = requests.get(get_url(ip, '/hms/rest/systems.php'))
    is_active = False
    if response.ok:
        data = response.json()
        hms = data.get('hms')
        is_active = hms['ha_activeDevice'] == '' and hms['ha_mode'] == '1'
    return is_active


def record(ip, params, data):
    headers = {
        'Content-type': 'application/json'
    }
    response = requests.put(get_url(ip, '/hms/rest/controls.php'), headers=headers, json=data, params=params)
    if response.ok:
        return response.json()
    else:
        return False


def get_info(ip):
    response = requests.get(get_url(ip, '/hms/rest/controls.php'))
    if response.ok:
        return response.json()
    return {}
