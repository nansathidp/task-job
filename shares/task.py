import os
from datetime import timedelta

from settings import config
from shares.service import MongoDB
from tasks.file import make_file
from utils import file

mongodb_client = MongoDB()


def generate_code(channel_code, time, ep_id, **kwargs):
    opt = ''
    for k, v in kwargs.items():
        opt = opt + k + '=' + v
    return '{}${}${}${}'.format(ep_id, channel_code, time, opt)


def create_body(label, code_init, scheduled_time, **kwargs):
    return {
        'label': label,
        'code': code_init,
        'runAt': scheduled_time.utcfromtimestamp(scheduled_time.timestamp()),
        'data': kwargs,
    }


def create_task(channel_code, start_time, end_time, episode_id):
    channel_code = channel_code.lower()
    path = config.TASK_PATH_ACTIVE
    start_time = start_time + timedelta(seconds=2)
    start_time_string = start_time.strftime(config.DATETIME_FORMAT_FILE)
    end_time_string = end_time.strftime(config.DATETIME_FORMAT_FILE)
    name = '{}_{}_{}_{}'.format(episode_id, channel_code, start_time_string, end_time_string)
    file_name = file.file_name(path, name)
    if not os.path.exists(file_name) and channel_code:
        print('initial task {} is channel {}'.format(episode_id, channel_code))
        # redis = Redis(config.RQ.get('HOST'), config.RQ.get('PORT'))
        # schedule = TaskScheduler(connection=redis)
        start_timestamps = start_time
        stop_timestamps = end_time
        stop_time = end_time - timedelta(seconds=config.TIME_OUT_STOP_RECORD)
        # job_start = schedule.enqueue_at(start_time, action, channel_code, True)
        # job_stop = schedule.enqueue_at(stop_time, action, channel_code, False)
        # job_move = schedule.enqueue_at(stop_time, move_file, name)
        # uuid_list = [job_start.get_id(), job_stop.get_id(), job_move.get_id()]
        # job_overall_uuid = '$'.join(uuid_list)
        code_start = generate_code(channel_code, start_time_string, episode_id, action='start', code=channel_code)
        code_stop = generate_code(channel_code, end_time_string, episode_id, action='stop', code=channel_code)
        code_status_start = generate_code(channel_code, start_time_string, episode_id, status='record',
                                          code=channel_code)
        code_status_stop = generate_code(
            channel_code,
            end_time_string,
            episode_id,
            action='recorded',
            code=channel_code
        )

        body_start = create_body('action', code_start, start_time, code=channel_code, action='start')
        body_stop = create_body('action', code_stop, stop_time, code=channel_code, action='stop')
        body_status_start = create_body('update', code_status_start, start_time,
                                        status='record',
                                        code=channel_code,
                                        stream_code=channel_code,
                                        record_date=start_timestamps.utcfromtimestamp(start_timestamps.timestamp()),
                                        ep_id=episode_id
                                        )
        body_status_stop = create_body('update', code_status_stop, stop_time,
                                       status='recorded',
                                       code=channel_code,
                                       ep_id=episode_id,
                                       stream_code=channel_code,
                                       record_date=stop_timestamps.utcfromtimestamp(stop_timestamps.timestamp()),
                                       )
        jobs = mongodb_client.db.jobs.find(
            {'code': {'$in': [code_start, code_stop, code_status_start, code_status_stop]}})
        jobs = list(jobs)
        if len(jobs) is 0:
            mongodb_client.db.jobs.insert_one(body_start)
            mongodb_client.db.jobs.insert_one(body_stop)
            mongodb_client.db.jobs.insert_one(body_status_start)
            mongodb_client.db.jobs.insert_one(body_status_stop)
            mongodb_client.db.live2File_history.insert_one({})
            make_file(file_name, '{}${}${}${}'.format(code_start, code_stop, code_status_start, code_status_stop))

    else:
        print('task duplicate')
