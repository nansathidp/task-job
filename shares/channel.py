from datetime import datetime

from settings import config
from shares.service import Cached, MongoDB


def set_action_channel():
    mongodb_client = MongoDB()
    channel_record_list = mongodb_client.db.content_channel.find({'live_to_vod': {'$in': ['all', 'select']}})
    cached = Cached()
    cached.memory.set('channel_record', list(channel_record_list))
    data_list = cached.memory.get('channel_record')
    if data_list.__len__() > 0:
        print('%s commit channel record.' % datetime.now().strftime('%c'))


def set_info(information_list):
    cached = Cached()
    cached.memory.set('information_all', information_list)
    for device in information_list:
        for channel_index, channel_information in enumerate(device.get('ch_info', [])):
            full_name = channel_information.get('name', '')
            full_names = full_name.split('_')
            if len(full_names) > 0:
                code = full_names[0]
                if 'Standby' in code:
                    continue
                if 'LiveEvent' in code:
                    continue
                value = {
                    'channel_index': channel_index,
                    'channel_id': channel_information.get('id'),
                    'device_id': device.get('id'),
                    'is_recording': bool(channel_information['isrecording']),
                    'ip_manage': cached.memory.get('ip_active', config.TRANSCODE_IP[0])
                }
                cached.memory.set(key=code, value=value)
