import time
from datetime import datetime

from shares import trascode, channel


def run():
    index = 0
    while True:
        ip = '10.18.12.27'
        if trascode.check_active(ip):
            trascode.set_active_ip(ip)
            information = trascode.get_info(ip)
            channel.set_info(information)
            channel.set_action_channel()
            print('IP: {} (Active) {}'.format(ip, datetime.now().strftime('%c')))
            time.sleep(60)
        else:
            index = 1 if index == 0 else 0
            trascode.clear()
            print('IP: {} (In Active) {}'.format(ip, datetime.now().strftime('%c')))
            print('To next site')
            time.sleep(3)


if __name__ == '__main__':
    try:
        print('Start fetch channel....')
        run()
    except KeyboardInterrupt:
        print('Stop fetch channel....')
