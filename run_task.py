from shares import interval, epg


def main():
    import time

    # epg.run()
    interval_time = interval.Interval(1, epg.run)
    print("Starting Interval initial epg, press CTRL+C to stop.", interval_time.start())

    while True:
        try:
            time.sleep(0.1)
        except KeyboardInterrupt:
            print("Shutting down interval ...", interval_time.stop())
            break


if __name__ == '__main__':
    main()
